# RetroAchievementsAPIViewer

RetroAchievements parser / viewer / playlist manager

## Description

This is a small personal project that synchronizes a local database with RetroAchievements' one.

The few included features are:
- __Full local database synchronization__ using RetroAchievements API
- __Lighter local database update__ using a combination of RetroAchievements HTML pages parsing & API
- __DataTables easy visualization__ with filtering and ordering
- __RetroArch playlist updater__ to automatically create duplicates of your personal RetroArch playlists, but only with games having achievements (and optionally some special global ones, like "Last Added")

## Why?!

RetroAchievement API is very basic and is based on game hashes to retrieve achievements, as this makes sense for an accurate match.

Because of that it's not possible to directly see all the games you have that do have achievements, and even less possible to quickly (and often) update it.

__This project is intended to make a very simpler copy of RetroAchievement API in local database to provide a way around those missing functionalities.__

It also provides a clean web page & API with basic filters and ordering, that are mainly missing from RA website (for example be able to see the last Games that had their first achievement - basically last games added on RA).

## Limitations

Because of the project design, __playlist parsing__ isn't a perfect match as it's only based only on the Console _(playlist name)_ and the Game name _(a "cleaned" one to maximize results)_. It might (and should) have false positives, but false positives should only indicate you don't have the required exact version (hash) of the game.

Also keep in mind that updating achievements requires one API call per (updated) game.

## Requirements

- PHP 8.0.2 or higher, with common PHP extensions _(which are installed and enabled by default in most PHP 8 installations)_: Ctype, iconv, PCRE, Session, SimpleXML, Tokenizer, etc.
- A local database (MySQL or other)
- [Composer](https://getcomposer.org/download/)
- [RetroAchievement](https://retroachievements.org/) account for API credentials

## Getting started

```
composer install
yarn install && yarn build
```

Set your local env variables including RetroAchievements credentials (for example in `.env.local`):
```
DATABASE_URL="mysql://db_user:db_password@127.0.0.1:3306/db_name?serverVersion=5.7&charset=utf8mb4"
RA_USER=your_ra_username
RA_API_KEY=your_ra_api_key
```

Create your database structure:
```
php bin/console doctrine:schema:create
php bin/console doctrine:migration:migrate
```

As you don't have any database yet you should start by doing a full synchronization __(⚠️ Be aware that it might take about 2 hours, and do many RA API requests)__:
```
php bin/console app:retroachivement:full-refresh
```

Then you can quickly update your database anytime using:
```
php bin/console app:retroachivement:light-refresh
```

## Usage

To view & search easily in your local database you can use the DaTables page, pointing your webserver to `/public` (see [screenshots](#user-content-screenshot)).


And finally, if you want to automatically create (prefixed) achievements-only RetroArch playlists by parsing your own playlists:
```
php bin/console app:retroachivement:epur-game-names
php bin/console app:retroachivement:update-playlist
```
The first one creates simpler cleaned game names in your local database to be able to improve game matching.

The second one will prompt you questions to adapt the process to your needs, __however it assumes your local machine isn't the one with RetroArch thus it uses SSH to retrieve/upload playlists (originally made for Lakka)__.

You can run those and the light update easily using the default parameters with:
```
./update.sh
```

## Screenshot

![RetroAchievement API DataTables](/screenshots/datatables.png?raw=true "RetroAchievement API DataTables")
![RetroAchievement command database update light](/screenshots/cmd-update-light.png?raw=true "RetroAchievement command database update light")

## TODO

 - [x] Better documentation
 - [ ] Remove `yarn`
 - [ ] Select which Consoles you wish to update _(mainly for a quicker first "full" database synchronization)_
 - [ ] Add Game status _(present/missing in your RetroArch game library)_ in DataTables view & filters
 - [ ] Custom playlist with recently played updated games
 - [ ] Handle deleted achievements somehow
 - [ ] Support more use cases?

## Project status

It's only a personal Proof Of Concept and I don't specially plan to update it, however I'm always open to questions/suggestions or directly Pull Requests.
