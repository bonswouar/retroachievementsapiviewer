<?php

namespace App\Command;

use App\Entity\Console;
use App\Entity\Game;
use App\Service\RetroAchievementsService;
use App\Trait\EpurStrTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ConfirmationQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;
use Symfony\Component\Filesystem\Exception\IOExceptionInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Filesystem\Path;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Process\Exception\ProcessFailedException;
use Symfony\Component\Process\Process;

#[AsCommand(
    name: 'app:retroachivement:update-playlist',
    description: 'Refresh retroachivements local database parsing RA HTTP pages',
)]
class RetroachivementsUpdatePlaylistCommand extends Command
{
    use EpurStrTrait;

    private EntityManagerInterface $em;
    private RetroAchievementsService $raService;
    private $envSshHost;
    private Crawler $crawler;
    private $io;
    private $nbAddedConsoles = 0;
    private $nbAddedGames = 0;
    private $nbAddedAchievements = 0;
    private $nbUpdatedAchievements = 0;
    private OutputInterface $output;
    private $customPlaylists = ['top' => [], 'last' => []];
    private $consoles = [];

    private const CUSTOM_PLAYLIST_MAX_SIZE = 200;

    // Link for console names between RetroAchievements API & RetroArch playlist
    private const CONSOLES_CORRESPONDANCES = [
        'FBNeo - Arcade Games' => 'Arcade',
        'Nintendo - Nintendo Entertainment System' => 'NES',
        'Nintendo - Super Nintendo Entertainment System' => 'SNES',
        'Microsoft - MSX2' => 'MSX',
        'NEC - PC Engine - TurboGrafx 16' => 'PC Engine',
        'Bandai - WonderSwan Color' => 'WonderSwan',
        'NEC - PC Engine SuperGrafx' => 'PC Engine',
        'NEC - PC Engine CD - TurboGrafx-CD' => 'PC Engine',
        'Sega - Mega Drive - Genesis' => 'Mega Drive',
        'Sega - Mega-CD - Sega CD' => 'Sega CD',
        'Magnavox - Odyssey2' => 'Magnavox Odyssey 2',
        'The 3DO Company - 3DO' => '3DO Interactive Multiplayer',
        'Sony - PlayStation Portable (PSN)' => 'PlayStation Portable',
        'SNK - Neo Geo Pocket Color' => 'Neo Geo Pocket',
        'Sega - Master System - Mark III' => 'Master System',
        'WASM-4' => 'WASM-4', // ??
    ];

    public function __construct(EntityManagerInterface $em, RetroAchievementsService $raService, $sshHost = null)
    {
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em = $em;
        $this->raService = $raService;
        $this->envSshHost = $sshHost;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'yes',
                null,
                InputOption::VALUE_NONE,
                'Answer default value to all Y/N interactive questions',
            )
            ->addOption(
                'host',
                null,
                InputOption::VALUE_REQUIRED,
                'SSH host',
                null
            )
        ;
    }

    private function getCustomPlaylistTemplate()
    {
        return [
            'version' => '1.5',
            'default_core_path' => '',
            'default_core_name' => '',
            'label_display_mode' => 0,
            'right_thumbnail_mode' => 0,
            'left_thumbnail_mode' => 0,
            'sort_mode' => 2,
            'items' => [],
        ];
    }

    private function getMissingGamePlaylistTemplate()
    {
        return [
            'path' => '',
            'entry_slot' => null,
            'label' => null,
            'core_path' => 'DETECT',
            'core_name' => 'DETECT',
            'crc32' => '00000000|crc',
            'db_name' => '',
        ];
    }

    // type => "top" / "last"
    private function addFor(Game $game, array $gameArray, $type)
    {
        $added = false;
        if (count($this->customPlaylists[$type])) {
            foreach ($this->customPlaylists[$type] as $key => $elem) {
                $lastAddedGame = $elem['game'];
                if (('last' === $type && $lastAddedGame->getCreatedAt() < $game->getCreatedAt()) || ('top' === $type && $lastAddedGame->getTotalPoints() < $game->getTotalPoints())) {
                    $this->customPlaylists[$type] = array_merge(array_slice($this->customPlaylists[$type], 0, $key), [['game' => $game, 'gameArray' => $gameArray]], array_slice($this->customPlaylists[$type], $key));
                    $added = true;
                    break;
                }
            }
        }
        if (!$added) {
            $this->customPlaylists[$type][] = ['game' => $game, 'gameArray' => $gameArray];
        }
        $cleanGames = [];
        // Prevent duplicate games (just in case)
        foreach ($this->customPlaylists[$type] as $key => $elem) {
            $lastAddedGame = $elem['game'];
            if (!isset($cleanGames[$lastAddedGame->getName()])) {
                $cleanGames[$lastAddedGame->getName()] = true;
            } else {
                unset($this->customPlaylists[$type][$key]);
                break;
            }
        }
        if (count($this->customPlaylists[$type]) > self::CUSTOM_PLAYLIST_MAX_SIZE) {
            array_pop($this->customPlaylists[$type]);
        }
    }

    private function getMissingPlaylist($games, $playlistPath)
    {
        $missingGames = [];
        $missingGamesByName = [];
        foreach ($games as $game) {
            $console = $game->getConsole();
            if (!$this->gameExists($game->getName(), $console, $playlistPath)) {
                if (count($missingGames) >= self::CUSTOM_PLAYLIST_MAX_SIZE) {
                    break;
                }
                $template = $this->getMissingGamePlaylistTemplate();
                $template['label'] = $game->getName();
                if (isset($this->consoles[$console->getId()]) && isset($this->consoles[$console->getId()]['db_name'])) {
                    $template['db_name'] = $this->consoles[$console->getId()]['db_name'];
                } else {
                    $template['db_name'] = $console->getName();
                }
                if (!isset($missingGamesByName[$game->getName()])) {
                    $missingGames[] = $template;
                    $missingGamesByName[$game->getName()] = true;
                }
            }
        }

        return $missingGames;
    }

    private function addForCustomPlaylists(Game $game, array $gameArray)
    {
        $this->addFor($game, $gameArray, 'last');
        $this->addFor($game, $gameArray, 'top');
    }

    // Check if a specifi Game is in the playlists
    private function gameExists(string $gameName, Console $console, string $playlistPath): bool
    {
        $gameName = $this->epurStr($gameName);
        if (isset($this->consoles[$console->getId()])) {
            $data = file_get_contents($this->consoles[$console->getId()]['local_playlist_path']);
            $jsonData = json_decode($data, true);
            foreach ($jsonData['items'] as $gameItem) {
                $fullGameName = $gameItem['label'];
                $cleanedGameName = $this->epurStr($fullGameName);
                if (stristr($gameName, $cleanedGameName)) {
                    return true;
                }
            }
        }

        return false;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $this->output);
        $filesystem = new Filesystem();
        $yes = $input->getOption('yes');
        $sshHost = $input->getOption('host');
        $this->io->title('Retrieving distant playlists');

        if (!$sshHost) {
            if (!$sshHost = $this->envSshHost) {
                $helper = $this->getHelper('question');
                $question = new Question('SSH host (Lakka/RetroArch host): ');

                if (!$sshHost = $helper->ask($input, $output, $question)) {
                    $this->io->error('No SSH host provided.');

                    return Command::FAILURE;
                }
            }
        }

        $playlistPath = './playlists';
        if (!$yes) {
            $question = new Question(sprintf('Playlists server path [default: %s]: ', $playlistPath), $playlistPath);
            if (!$playlistPath = $helper->ask($input, $output, $question)) {
                return Command::FAILURE;
            }
        }

        $download = true;
        $tmpRootDir = Path::normalize(sys_get_temp_dir().'/ra_playlists_'.$sshHost);
        $tmpDir = Path::normalize($tmpRootDir.'/old');
        if ($filesystem->exists($tmpDir)) {
            if (!$yes) {
                $download = false;
                $question = new ConfirmationQuestion(sprintf('Local tmp dir exists, re-download playlists from %s [Y/n]? ', $sshHost), false);
                if ($download = $helper->ask($input, $output, $question)) {
                    $filesystem->remove($tmpDir);
                }
            }
        }
        try {
            $filesystem->mkdir($tmpDir);
        } catch (IOExceptionInterface $exception) {
            $this->io->error('An error occurred while creating your directory at '.$exception->getPath());

            return Command::FAILURE;
        }

        if ($download) {
            $this->io->info('Downloading...');

            $process = new Process(['scp', '-r', sprintf('%s:%s/*.lpl', $sshHost, $playlistPath), $tmpDir]);
            $process->run();
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            echo $process->getOutput();
            $this->io->success('Playlists downloaded.');
        }

        $finder = new Finder();
        $finder->files()->in($tmpDir);

        if (!$finder->hasResults()) {
            $this->io->caution('SCP has failed or you don\'t have any playlist.');

            return Command::FAILURE;
        }

        $this->io->title('Generating Achievements playlists');

        $prefix = 'ACHIEVEMENTS';
        if (!$yes) {
            $question = new Question(sprintf('New playlists prefix [default: %s]: ', $prefix), $prefix);
            if (!$prefix = $helper->ask($input, $output, $question)) {
                return Command::FAILURE;
            }
        }
        $prefix = $prefix.' - ';

        $newTmpDir = Path::normalize($tmpRootDir.'/new');
        if ($filesystem->exists($newTmpDir)) {
            $filesystem->remove($newTmpDir);
        }
        try {
            $filesystem->mkdir($newTmpDir);
        } catch (IOExceptionInterface $exception) {
            $this->io->error('An error occurred while creating your directory at '.$exception->getPath());

            return Command::FAILURE;
        }

        $gamesWithAchievements = [];
        $nbConsolesParsed = 0;
        $nbConsolesFailed = 0;
        $nbTotalEntries = 0;

        foreach ($finder as $file) {
            $absoluteFilePath = $file->getRealPath();
            $fileNameWithExtension = $file->getRelativePathname();
            $fileNameWithoutExtension = str_replace('.lpl', '', $fileNameWithExtension);
            if (str_contains($fileNameWithoutExtension, $prefix)) {
                // $this->io->info(sprintf('"%s" is already a RetroAchievements playlist.', $fileNameWithExtension));
                continue;
            }
            $this->io->info(sprintf('Parsing %s', $fileNameWithExtension));

            if (isset(self::CONSOLES_CORRESPONDANCES[$fileNameWithoutExtension])) {
                $consoleName = self::CONSOLES_CORRESPONDANCES[$fileNameWithoutExtension];
            } else {
                $matches = [];
                preg_match('/- (.*?).lpl$/', $fileNameWithExtension, $matches);
                if (!isset($matches[1]) || !$matches[1]) {
                    if (str_contains($fileNameWithExtension, '-')) {
                        ++$nbConsolesFailed;
                        $this->io->note(sprintf('Console name not found from playlist "%s".', $fileNameWithExtension));
                        continue;
                    }
                    $consoleName = $fileNameWithoutExtension;
                } else {
                    $consoleName = $matches[1];
                }
            }
            $console = $this->em->getRepository(Console::class)->findOneByName($consoleName);
            if (!$console) {
                $console = $this->em->getRepository(Console::class)->findOneByName(str_replace(' - ', ' ', $fileNameWithoutExtension));
                if (!$console) {
                    ++$nbConsolesFailed;
                    $this->io->note(sprintf('Console %s from playlist "%s" not found in database.', $consoleName, $fileNameWithExtension));
                    continue;
                }
            }

            $this->consoles[$console->getId()] = ['local_playlist_path' => $absoluteFilePath];
            ++$nbConsolesParsed;
            $newPlaylistFileNameWithExtension = $prefix.$fileNameWithExtension;

            $data = file_get_contents($absoluteFilePath);
            $jsonData = json_decode($data, true);
            $newPlaylistData = $jsonData;
            $newPlaylistData['items'] = [];

            $progressBar = new ProgressBar($this->output, count($jsonData['items']));
            foreach ($jsonData['items'] as $gameItem) {
                if (!isset($this->consoles[$console->getId()]['db_name'])) {
                    $this->consoles[$console->getId()]['db_name'] = $gameItem['db_name'];
                }

                ++$nbTotalEntries;
                $fullGameName = $gameItem['label'];
                $cleanedGameName = trim(preg_replace('@\(.*?\)@', '', $fullGameName));
                $cleanedEpurGameName = $this->epurStr($cleanedGameName);
                if ($game = $this->em->getRepository(Game::class)->findOneByNameAndConsole($cleanedGameName, $cleanedEpurGameName, $console)) {
                    if (count($game->getAchievements())) {
                        $newPlaylistData['items'][] = $gameItem;
                        $gamesWithAchievements[$game->getRaId()] = true;
                        $this->addForCustomPlaylists($game, $gameItem);
                    }
                }
                $progressBar->advance();
            }
            if (count($newPlaylistData['items'])) {
                $filepath = Path::normalize($newTmpDir.'/'.$newPlaylistFileNameWithExtension);
                $filesystem->dumpFile($filepath, json_encode($newPlaylistData));
            }
            $progressBar->finish();
            $this->io->newLine(2);
            $this->io->info(sprintf('%s Games with achievements found', count($newPlaylistData['items'])));
        }
        $this->io->success('Parsing done.');

        $customPlaylists = true;
        if (!$yes) {
            $question = new ConfirmationQuestion('Do you want create custom playlists (Global Top/Last Added) [Y/n]? ', true);
            $customPlaylists = $helper->ask($input, $output, $question);
        }

        if ($customPlaylists) {
            $this->io->title('Generating custom playlists');

            // custom playlists from existing games

            $jsonLastAdded = $this->getCustomPlaylistTemplate();
            foreach ($this->customPlaylists['last'] as $customElem) {
                $jsonLastAdded['items'][] = $customElem['gameArray'];
            }
            $filepath = Path::normalize($newTmpDir.'/'.$prefix.' Ah ! Last Added.lpl');
            $filesystem->dumpFile($filepath, json_encode($jsonLastAdded));

            $jsonTopPoints = $this->getCustomPlaylistTemplate();
            foreach ($this->customPlaylists['top'] as $customElem) {
                $jsonTopPoints['items'][] = $customElem['gameArray'];
            }
            $filepath = Path::normalize($newTmpDir.'/'.$prefix.' Ah ! Top points.lpl');
            $filesystem->dumpFile($filepath, json_encode($jsonTopPoints));

            // custom playlist for missing games
            $topPointsAll = $this->em->getRepository(Game::class)->findTopPoints(700);
            $topPointsMissing = $this->getMissingPlaylist($topPointsAll, $playlistPath);
            $jsonTopPointsMissing = $this->getCustomPlaylistTemplate();
            $jsonTopPointsMissing['items'] = $topPointsMissing;
            $filepath = Path::normalize($newTmpDir.'/'.$prefix.' Ahh ! Missing - Top Points.lpl');
            $filesystem->dumpFile($filepath, json_encode($jsonTopPointsMissing));

            $lastAddedMissing = [];
            $lastAddedAll = $this->em->getRepository(Game::class)->findLastAdded(700);
            $lastAddedMissing = $this->getMissingPlaylist($lastAddedAll, $playlistPath);
            $jsonLastAddedMissing = $this->getCustomPlaylistTemplate();
            $jsonLastAddedMissing['items'] = $lastAddedMissing;
            $filepath = Path::normalize($newTmpDir.'/'.$prefix.' Ahh ! Missing - Last Added.lpl');
            $filesystem->dumpFile($filepath, json_encode($jsonLastAddedMissing));

            $this->io->success('Custom playlists created.');
        }

        $this->io->info(sprintf('New playlists can be found in: %s', $newTmpDir));

        $this->io->success(sprintf('%s Games with achievements found in %s playlists, on a total of %s games (%s playlists had no Console correspondance)', count($gamesWithAchievements), $nbConsolesParsed, $nbTotalEntries, $nbConsolesFailed));

        $upload = true;
        if (!$yes) {
            $question = new ConfirmationQuestion('Do you want to upload the new playlists [Y/n]? ', true);
            $upload = $helper->ask($input, $output, $question);
        }
        if ($upload) {
            $this->io->title('Uploading');
            $finder = new Finder();
            $finder->files()->in($newTmpDir)->name($prefix.'*');

            if (!$finder->hasResults()) {
                $this->io->error('No new playlist found.');

                return Command::FAILURE;
            }
            $progressBar = new ProgressBar($this->output, count($finder));
            foreach ($finder as $file) {
                $absoluteFilePath = $file->getRealPath();
                $process = new Process(['scp', $absoluteFilePath, sprintf('%s:%s', $sshHost, $playlistPath)]);
                $process->run();
                if (!$process->isSuccessful()) {
                    throw new ProcessFailedException($process);
                }
                $progressBar->advance();
            }
            $progressBar->finish();
            $this->io->newLine(2);

            $this->io->success('Playlists uploaded.');
        }

        $restart = true;
        if (!$yes) {
            $question = new ConfirmationQuestion('Do you want to reload retroarch [Y/n]? ', true);
            $restart = $helper->ask($input, $output, $question);
        }
        if ($restart) {
            $process = new Process(['ssh', $sshHost, 'systemctl restart retroarch']);
            $process->run();
            if (!$process->isSuccessful()) {
                throw new ProcessFailedException($process);
            }
            echo $process->getOutput();
            $this->io->success('Retroarch restarted.');
        }

        return Command::SUCCESS;
    }
}
