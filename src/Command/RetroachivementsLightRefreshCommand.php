<?php

namespace App\Command;

use App\Entity\Achievement;
use App\Entity\Console;
use App\Entity\Game;
use App\Service\RetroAchievementsService;
use App\Trait\SafeFileGetContentsTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\DomCrawler\Crawler;

#[AsCommand(
    name: 'app:retroachivement:light-refresh',
    description: 'Refresh retroachivements local database parsing RA HTTP pages',
)]
class RetroachivementsLightRefreshCommand extends Command
{
    use SafeFileGetContentsTrait;

    private EntityManagerInterface $em;
    private RetroAchievementsService $raService;
    private Crawler $crawler;
    private $io;

    private $addedMinCreatedAt;
    private $nbAddedAchievements = 0;
    private $nbUpdatedAchievements = 0;
    private $addedAchievementsEmptyGames = [];

    private $cacheAddedGames = [];
    private $cacheAddedConsoles = [];
    private $cacheAddedAchievements = [];
    private OutputInterface $output;

    public function __construct(EntityManagerInterface $em, RetroAchievementsService $raService)
    {
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em = $em;
        $this->raService = $raService;

        parent::__construct();
    }

    // Parse recently added achievements
    private function parseNewAchievements($page, $progressBar): array
    {
        $progressBar->advance();

        $lastAchievements = [];
        $start = $page * 25;
        $html = $this->safe_file_get_contents(sprintf('https://retroachievements.org/achievementList.php?s=17&o=%s&p=3', $start));
        $crawler = new Crawler($html);
        $crawler = $crawler->filter('tr > td.w-full > span.inline > a');
        foreach ($crawler as $node) {
            if (str_contains($node->getAttribute('href'), '/achievement/')) {
                $raId = str_replace('https://retroachievements.org/achievement/', '', $node->getAttribute('href'));
                if ($this->em->getRepository(Achievement::class)->findOneByRaId($raId)) {

                    return $lastAchievements;
                }
                if (!in_array($raId, $lastAchievements)) {
                    $lastAchievements = array_merge($lastAchievements, [$raId]);
                }
            }
        }
        $lastAchievements = array_merge($lastAchievements, $this->parseNewAchievements($page + 1, $progressBar));

        return $lastAchievements;
    }

    // Parse recently updated achievements
    private function parseUpdatedAchievements($page, $progressBar): array
    {
        $progressBar->advance();

        $lastAchievements = [];

        $start = $page * 25;
        $html = $this->safe_file_get_contents(sprintf('https://retroachievements.org/achievementList.php?s=18&o=%s&p=3', $start));
        $crawler = new Crawler($html);
        $crawler = $crawler->filter('div.detaillist > div.table-wrapper > table > tbody > tr');

        foreach ($crawler as $node) {
            $crawlerHref = new Crawler($node);
            $crawlerHref = $crawlerHref->filter('span.inline > a');
            foreach ($crawlerHref as $nodeHref) {
                if (str_contains($nodeHref->getAttribute('href'), '/achievement/')) {
                    $raId = str_replace('https://retroachievements.org/achievement/', '', $nodeHref->getAttribute('href'));
                    if ($achievement = $this->em->getRepository(Achievement::class)->findOneByRaId($raId)) {
                        $lastDate = null;
                        $crawlerUpdatedAt = new Crawler($node);
                        $crawlerUpdatedAt = $crawlerUpdatedAt->filter('td > span.smalldate');
                        foreach ($crawlerUpdatedAt as $nodeUpdatedAt) {
                            $date = \DateTime::createFromFormat('d M Y, H:i', $nodeUpdatedAt->textContent);
                            // To prevent errors use only the most recent date
                            if ($date && $date > $lastDate) {
                                $lastDate = $date;
                            }
                        }
                        if ($achievement->getUpdatedAt()->format('Y-m-d H:i') === $lastDate->format('Y-m-d H:i') && ($achievement->getCreatedAt() < $this->addedMinCreatedAt || !$this->addedMinCreatedAt)) {

                            return $lastAchievements;
                        } else {
                            $lastAchievements = array_merge($lastAchievements, [$raId]);
                        }
                    }
                    $lastAchievements = array_merge($lastAchievements, [$raId]);
                }
            }
        }

        $lastAchievements = array_merge($lastAchievements, $this->parseUpdatedAchievements($page + 1, $progressBar));

        return $lastAchievements;
    }

    private function updateAchievements(array $achievements, $checkExists = true)
    {
        $progressBar = new ProgressBar($this->output, count($achievements));
        $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');

        foreach ($achievements as $achievementRaId) {
            $gameRaId = null;
            $achievementHtml = $this->safe_file_get_contents(sprintf('https://retroachievements.org/achievement/%s', $achievementRaId));
            $crawler = new Crawler($achievementHtml);
            $crawler = $crawler->filter('div.navpath > a');
            foreach ($crawler as $node) {
                if (str_contains($node->getAttribute('href'), '/game/')) {
                    $gameRaId = str_replace('/game/', '', $node->getAttribute('href'));
                }
            }
            if (!$gameRaId) {
                $this->io->newLine();
                $this->io->caution(sprintf('%s does not seem to be a valid Achievement ID', $achievementRaId));
                continue;
            }
            $gamesExtendedArray = $this->raService->getGameInfoExtended($gameRaId);
            $game = $this->cacheAddedGames[$gameRaId] ?? null;
            if (!$game && !$game = $this->em->getRepository(Game::class)->findOneByRaId($gameRaId)) {
                $gameName = $gamesExtendedArray['Title'];
                $consoleRaId = $gamesExtendedArray['ConsoleID'];
                $consoleName = $gamesExtendedArray['ConsoleName'];

                $console = $this->cacheAddedConsoles[$consoleRaId] ?? null;
                if (!$console && !$console = $this->em->getRepository(Console::class)->findOneByRaId($consoleRaId)) {
                    $console = new Console();
                    $console->setRaId($consoleRaId);
                    $console->setName($consoleName);
                    $this->em->persist($console);
                    $this->cacheAddedConsoles[$consoleRaId] = $console;
                }
                $game = new Game();
                $game->setRaId($gameRaId);
                $game->setName($gameName);
                $game->setConsole($console);
                $this->em->persist($game);
                $this->cacheAddedGames[$gameRaId] = $game;
            }

            $achievementsArray = $gamesExtendedArray['Achievements'];
            if (!isset($achievementsArray[$achievementRaId])) {
                $this->io->newLine();
                $this->io->caution(sprintf('%s does not seem to be a valid Achievement ID of Game %s. Valid Achievement IDs are: %s', $achievementRaId, $gameRaId, implode(', ', array_keys($achievementsArray))));
                continue;
            }
            $achievementArray = $achievementsArray[$achievementRaId];

            $achievementId = $achievementArray['ID'];
            $name = $achievementArray['Title'];
            $createdAt = \DateTime::createFromFormat('Y-m-d H:i:s', $achievementArray['DateCreated']);
            $updatedAt = \DateTime::createFromFormat('Y-m-d H:i:s', $achievementArray['DateModified']);
            $points = $achievementArray['Points'];
            if (!$checkExists || !$achievement = $this->em->getRepository(Achievement::class)->findOneByRaId($achievementId)) {
                if (0 === count($game->getAchievements())) {
                    $this->addedAchievementsEmptyGames[$game->getRaId()] = true;
                }
                $achievement = new Achievement();
                $achievement->setRaId($achievementId);
                $achievement->setGame($game);
                ++$this->nbAddedAchievements;
            } else {
                ++$this->nbUpdatedAchievements;
            }
            $achievement->setName($name);
            $achievement->setPoints((int) $points);
            $achievement->setCreatedAt($createdAt);
            if (!$this->addedMinCreatedAt || $createdAt < $this->addedMinCreatedAt) {
                $this->addedMinCreatedAt = $createdAt;
            }
            $achievement->setUpdatedAt($updatedAt);
            $this->em->persist($achievement);
            $progressBar->advance();
        }
        $progressBar->finish();
        $this->em->flush();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->output = $output;
        $this->io = new SymfonyStyle($input, $this->output);

        // Test api
        try {
            $this->raService->getConsoleIDs();
        } catch (\Exception $e) {
            $this->io->error('Can\'t connect to RA API');

            return Command::FAILURE;
        }

        $this->io->title('Parsing new achievements');

        $progressBar = new ProgressBar($this->output);
        $progressBar->setFormat(' Parsing page: %current% [%bar%] %elapsed:6s% %memory:6s%');
        $newAchievements = $this->parseNewAchievements(0, $progressBar);
        $progressBar->finish();

        if (count($newAchievements)) {
            $this->io->newLine();
            $this->io->title('Adding achievements');
            $this->updateAchievements(array_reverse(array_unique($newAchievements)), false);
        }
        $this->io->newLine();
        if ($this->nbAddedAchievements) {
            $message = sprintf('%s Consoles added, %s Games and %s achievements (including %s games that had no achievement before!)', count($this->cacheAddedConsoles), count($this->cacheAddedGames), $this->nbAddedAchievements, count($this->addedAchievementsEmptyGames));
        } else {
            $message = "Up-to-date";
        }
        $this->io->success($message);

        $this->nbAddedAchievements = 0;
        $this->nbUpdatedAchievements = 0;

        $this->io->title('Parsing updated achievements');

        $progressBar = new ProgressBar($this->output);
        $progressBar->setFormat(' Page %current% [%bar%] %elapsed:6s% %memory:6s%');
        $updatedAchievements = $this->parseUpdatedAchievements(0, $progressBar);
        $progressBar->finish();

        if (count($updatedAchievements)) {
            $this->io->newLine();
            $this->io->title('Updating achievements');
            $this->updateAchievements(array_reverse(array_unique($updatedAchievements)), true);
        }
        $this->io->newLine();
        if ($this->nbUpdatedAchievements) {
            $message = sprintf('%s achievements updated', $this->nbUpdatedAchievements);
        } else {
            $message = "Up-to-date";
        }
        $this->io->success($message);

        return Command::SUCCESS;
    }
}
