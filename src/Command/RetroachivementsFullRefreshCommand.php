<?php

namespace App\Command;

use App\Entity\Achievement;
use App\Entity\Console;
use App\Entity\Game;
use App\Service\RetroAchievementsService;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Style\SymfonyStyle;

#[AsCommand(
    name: 'app:retroachivement:full-refresh',
    description: 'Full refresh of retroachivements local database using RA API',
)]
class RetroachivementsFullRefreshCommand extends Command
{
    private EntityManagerInterface $em;
    private RetroAchievementsService $raService;

    public function __construct(EntityManagerInterface $em, RetroAchievementsService $raService)
    {
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em = $em;
        $this->raService = $raService;

        parent::__construct();
    }

    protected function configure(): void
    {
        $this
            ->addOption(
                'console',
                null,
                InputOption::VALUE_REQUIRED,
                'Console',
                null
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $io = new SymfonyStyle($input, $output);
        try {
            $consolesArray = $this->raService->getConsoleIDs();
        } catch (\Exception $e) {
            $io->error('Can\'t connect to RA API');

            return Command::FAILURE;
        }
        $consoleName = $input->getOption('console');

        $nbAddedConsoles = 0;
        $nbAddedGames = 0;
        $nbAddedAchievements = 0;

        foreach ($consolesArray as $consoleArray) {
            $nbAddedGamesConsole = 0;
            $nbAddedAchievementsConsole = 0;
            $consoleId = $consoleArray['ID'];
            $name = $consoleArray['Name'];

            if ($consoleName && mb_strtolower($name) !== mb_strtolower($consoleName)) {
                continue;
            }

            if (!$console = $this->em->getRepository(Console::class)->findOneByRaId($consoleId)) {
                $console = new Console();
                $console->setRaId($consoleId);
                $console->setName($name);
                $this->em->persist($console);
                ++$nbAddedConsoles;
            }

            $gamesArray = $this->raService->getGameList($consoleId);
            $progressBar = new ProgressBar($output, count($gamesArray));
            $progressBar->setFormat(' %current%/%max% [%bar%] %percent:3s%% %elapsed:6s%/%estimated:-6s% %memory:6s%');

            $progressBar->start();
            foreach ($gamesArray as $gameArray) {
                $gameId = $gameArray['ID'];
                $name = $gameArray['Title'];
                if (!$game = $this->em->getRepository(Game::class)->findOneByRaId($gameId)) {
                    $game = new Game();
                    $game->setRaId($gameId);
                    $game->setName($name);
                    $game->setConsole($console);
                    $this->em->persist($game);
                    ++$nbAddedGamesConsole;
                }

                $gamesExtendedArray = $this->raService->getGameInfoExtended($gameId);
                $achievementsArray = $gamesExtendedArray['Achievements'];

                if (count($achievementsArray)) {
                    foreach ($achievementsArray as $achievementArray) {
                        $achievementId = $achievementArray['ID'];
                        $name = $achievementArray['Title'];
                        $createdAt = \DateTime::createFromFormat('Y-m-d H:i:s', $achievementArray['DateCreated']);
                        $updatedAt = \DateTime::createFromFormat('Y-m-d H:i:s', $achievementArray['DateModified']);
                        $points = $achievementArray['Points'];
                        if (!$achievement = $this->em->getRepository(Achievement::class)->findOneByRaId($achievementId)) {
                            $achievement = new Achievement();
                            $achievement->setRaId($achievementId);
                            $achievement->setName($name);
                            $achievement->setPoints((int) $points);
                            $achievement->setCreatedAt($createdAt);
                            $achievement->setUpdatedAt($updatedAt);
                            $achievement->setGame($game);
                            $this->em->persist($achievement);
                            ++$nbAddedAchievementsConsole;
                        }
                    }
                }
                $this->em->flush();
                $progressBar->advance();
            }
            $this->em->flush();
            $io->info(sprintf('%s achievements and %s games added for %s', $nbAddedAchievementsConsole, $nbAddedGamesConsole, $console->getName()));
            $this->em->clear();
            gc_collect_cycles();
            $nbAddedAchievements += $nbAddedAchievementsConsole;
            $nbAddedGames += $nbAddedGamesConsole;
        }
        $this->em->flush();

        $io->success(sprintf('%s Consoles added, %s Games, and %s achievements', $nbAddedConsoles, $nbAddedGames, $nbAddedAchievements));

        return Command::SUCCESS;
    }
}
