<?php

namespace App\Command;

use App\Entity\Game;
use App\Service\RetroAchievementsService;
use App\Trait\EpurStrTrait;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Attribute\AsCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

#[AsCommand(
    name: 'app:retroachivement:epur-game-names',
    description: 'TMP',
)]
class RetroachivementsEpurGameNamesCommand extends Command
{
    use EpurStrTrait;

    private EntityManagerInterface $em;

    public function __construct(EntityManagerInterface $em, RetroAchievementsService $raService, $sshHost = null)
    {
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        $this->em = $em;

        parent::__construct();
    }

    protected function execute(InputInterface $input, OutputInterface $output): int
    {
        $games = $this->em->getRepository(Game::class)->findAll();
        foreach ($games as $game) {
            $game->setEpurName($this->epurStr($game->getName()));
        }
        $this->em->flush();

        return Command::SUCCESS;
    }
}
