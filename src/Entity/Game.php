<?php

namespace App\Entity;

use App\Repository\GameRepository;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Serializer\Annotation\Groups;

#[ORM\Entity(repositoryClass: GameRepository::class)]
class Game
{
    #[ORM\Id]
    #[ORM\GeneratedValue]
    #[ORM\Column(type: 'integer')]
    private $id;

    #[Groups(['games_datatable'])]
    #[ORM\Column(type: 'integer', unique: true)]
    private $raId;

    #[ORM\Column(type: 'string', length: 255)]
    #[Groups(['games_datatable'])]
    private $name;

    #[ORM\ManyToOne(targetEntity: Console::class, inversedBy: 'games')]
    #[ORM\JoinColumn(nullable: false)]
    private $console;

    #[ORM\OneToMany(mappedBy: 'game', targetEntity: Achievement::class, orphanRemoval: true, fetch: 'EXTRA_LAZY')]
    private $achievements;

    #[ORM\Column(type: 'string', length: 255, nullable: true)]
    private $epurName;

    public function __construct()
    {
        $this->achievements = new ArrayCollection();
    }

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getRaId(): ?int
    {
        return $this->raId;
    }

    public function setRaId(int $raId): self
    {
        $this->raId = $raId;

        return $this;
    }

    public function getName(): ?string
    {
        return $this->name;
    }

    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    public function getConsole(): ?Console
    {
        return $this->console;
    }

    public function setConsole(?Console $console): self
    {
        $this->console = $console;

        return $this;
    }

    /**
     * @return Collection<int, Achievement>
     */
    public function getAchievements(): Collection
    {
        return $this->achievements;
    }

    public function addAchievement(Achievement $achievement): self
    {
        if (!$this->achievements->contains($achievement)) {
            $this->achievements[] = $achievement;
            $achievement->setGame($this);
        }

        return $this;
    }

    public function removeAchievement(Achievement $achievement): self
    {
        if ($this->achievements->removeElement($achievement)) {
            // set the owning side to null (unless already changed)
            if ($achievement->getGame() === $this) {
                $achievement->setGame(null);
            }
        }

        return $this;
    }

    #[Groups(['games_datatable'])]
    public function getNbAchievements()
    {
        return count($this->getAchievements());
    }

    #[Groups(['games_datatable'])]
    public function getConsoleName()
    {
        return $this->getConsole()->getName();
    }

    #[Groups(['games_datatable'])]
    public function getCreatedAt()
    {
        $min = null;
        foreach ($this->getAchievements() as $achievement) {
            if (!$min || $achievement->getCreatedAt() < $min) {
                $min = $achievement->getCreatedAt();
            }
        }

        return $min;
    }

    #[Groups(['games_datatable'])]
    public function getUpdatedAt()
    {
        $max = null;
        foreach ($this->getAchievements() as $achievement) {
            if (!$max || $achievement->getUpdatedAt() > $max) {
                $max = $achievement->getUpdatedAt();
            }
        }

        return $max;
    }

    #[Groups(['games_datatable'])]
    public function getTotalPoints()
    {
        $points = 0;
        foreach ($this->getAchievements() as $achievement) {
            $points += $achievement->getPoints();
        }

        return $points;
    }

    public function getAchievementEntityCount()
    {
        return count($this->achievements);
    }

    public function getEpurName(): ?string
    {
        return $this->epurName;
    }

    public function setEpurName(?string $epurName): self
    {
        $this->epurName = $epurName;

        return $this;
    }
}
