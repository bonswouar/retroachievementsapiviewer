<?php

namespace App\Controller;

use App\Repository\ConsoleRepository;
use App\Repository\GameRepository;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\SerializerInterface;

class AchievementController extends AbstractController
{
    #[Route('/', name: 'app_achievement')]
    public function index(Request $request, GameRepository $gameRepository, ConsoleRepository $consoleRepository): Response
    {
        $games = $gameRepository->findAll();
        $consoles = $consoleRepository->findAll();

        return $this->render('achievement/index.html.twig', [
            'games' => $games,
            'consoles' => $consoles,
        ]);
    }

    #[Route('/api', name: 'app_api')]
    public function api(Request $request, GameRepository $gameRepository, SerializerInterface $serializer)
    {
        $draw = $request->request->get('draw');
        $row = $request->request->get('start');
        $rowperpage = $request->request->get('length');
        $columnIndex = $request->request->all('order')[0]['column'];
        $columnName = $request->request->all('columns')[$columnIndex]['data'];
        $columnSortOrder = $request->request->all('order')[0]['dir'];
        $searchValue = $request->request->all('search')['value'];
        $hideEmpty = $request->request->get('hideEmpty');
        $hideHacks = $request->request->get('hideHacks');
        $hideEvents = $request->request->get('hideEvents');
        $consoleId = $request->request->get('consoleId');

        $games = $gameRepository->findDatatable($row, $rowperpage, $columnName, $columnSortOrder, $searchValue, $hideEmpty, $hideHacks, $hideEvents, $consoleId);
        $totalRecordwithFilter = $gameRepository->countAll($searchValue, $hideEmpty, $hideHacks, $hideEvents, $consoleId);
        $totalRecords = $gameRepository->countAll(null);

        $data = [
          'draw' => intval($draw),
          'iTotalRecords' => $totalRecords,
          'iTotalDisplayRecords' => $totalRecordwithFilter,
          'aaData' => $games,
        ];
        $json = $serializer->serialize($data, 'json', ['groups' => 'games_datatable']);

        return JsonResponse::fromJsonString($json);
    }
}
