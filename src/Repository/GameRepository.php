<?php

namespace App\Repository;

use App\Entity\Console;
use App\Entity\Game;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\ORM\OptimisticLockException;
use Doctrine\ORM\ORMException;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @extends ServiceEntityRepository<Game>
 *
 * @method Game|null find($id, $lockMode = null, $lockVersion = null)
 * @method Game|null findOneBy(array $criteria, array $orderBy = null)
 * @method Game[]    findAll()
 * @method Game[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class GameRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Game::class);
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function add(Game $entity, bool $flush = true): void
    {
        $this->_em->persist($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    /**
     * @throws ORMException
     * @throws OptimisticLockException
     */
    public function remove(Game $entity, bool $flush = true): void
    {
        $this->_em->remove($entity);
        if ($flush) {
            $this->_em->flush();
        }
    }

    // /**
    //  * @return Game[] Returns an array of Game objects
    //  */
    /*
    public function findByExampleField($value)
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->orderBy('g.id', 'ASC')
            ->setMaxResults(10)
            ->getQuery()
            ->getResult()
        ;
    }
    */

    /*
    public function findOneBySomeField($value): ?Game
    {
        return $this->createQueryBuilder('g')
            ->andWhere('g.exampleField = :val')
            ->setParameter('val', $value)
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }
    */

    public function findAll()
    {
        return $this->createQueryBuilder('g')
            // ->setMaxResults(50)
            ->orderBy('g.name', 'ASC')
            ->getQuery()
            ->getResult()
        ;
    }

    private function getSpecialFields(): array
    {
        return ['nbAchievements', 'createdAt', 'updatedAt', 'totalPoints', 'consoleName'];
    }

    private function addSearch($searchValue, $qb)
    {
        $qb
            ->andWhere('g.name LIKE :searchValue OR c.name LIKE :searchValue')
            ->setParameter('searchValue', '%'.$searchValue.'%')
        ;
    }

    public function findDatatable($row, $rowperpage, $columnName, $columnSortOrder, $searchValue, $hideEmpty = false, $hideHacks = false, $hideEvents = false, $consoleId = null)
    {
        $qb = $this->createQueryBuilder('g')
            ->setFirstResult($row)
            ->join('g.console', 'c')
            ->leftJoin('g.achievements', 'a')
            ->groupBy('g.id')
            ->setMaxResults($rowperpage)
        ;

        if ($hideEmpty) {
            $qb->andWhere('a.id is not null');
        }
        if ($hideHacks) {
            $qb
                ->andWhere('g.name NOT LIKE :hack and g.name not like :homebrew and g.name not like :demo and g.name not like :prototype and g.name not like :unlicensed and g.name not like :test and g.name not like :subset')
                ->setParameter('hack', '%hack%')
                ->setParameter('homebrew', '%~Homebrew~%')
                ->setParameter('demo', '%~demo~%')
                ->setParameter('prototype', '%~prototype~%')
                ->setParameter('unlicensed', '%~unlicensed~%')
                ->setParameter('test', '%~Test Kit~%')
                ->setParameter('subset', '%[Subset - %')
            ;
        }
        if ($hideEvents) {
            $qb
                ->andWhere('c.name NOT LIKE :event')
                ->setParameter('event', '%event%')
            ;
        }
        if ($consoleId) {
            $qb
                ->andWhere('c.id = :consoleId')
                ->setParameter('consoleId', $consoleId)
            ;
        }

        if ($searchValue) {
            $this->addSearch($searchValue, $qb);
        }
        switch ($columnName) {
            case 'nbAchievements':
                $qb->addSelect('COUNT(a.id) as nbAchievements');
                break;
            case 'createdAt':
                $qb->addSelect('MIN(a.createdAt) as createdAt');
                break;
            case 'updatedAt':
                $qb->addSelect('MAX(a.updatedAt) as updatedAt');
                break;
            case 'totalPoints':
                $qb->addSelect('SUM(a.points) as totalPoints');
                break;
            case 'consoleName':
                $qb->addSelect('c.name as consoleName');
        }

        if (in_array($columnName, $this->getSpecialFields())) {
            $qb
                ->addOrderBy($columnName, $columnSortOrder)
                ->addOrderBy('g.id', 'ASC')
            ;
        } else {
            $qb
                ->addSelect('MIN(a.createdAt) as createdAt')
                ->addOrderBy('g.'.$columnName, $columnSortOrder)->addOrderBy('createdAt', 'DESC')
            ;
        }
        $result = [];
        foreach ($qb->getQuery()->getResult() as $dat) {
            $result[] = $dat[0];
        }

        return $result;
    }

    public function countAll($searchValue, $hideEmpty = false, $hideHacks = false, $hideEvents = false, $consoleId = null)
    {
        $qb = $this->createQueryBuilder('g')
            // ->select('count(g.id) as nb, a.id')
            ->join('g.console', 'c')
            ->leftJoin('g.achievements', 'a')
            ->groupBy('g.id')
        ;
        if ($hideEmpty) {
            $qb
                ->andWhere('a.id is not null')
            ;
        }
        if ($hideHacks) {
            $qb
                ->andWhere('g.name NOT LIKE :hack')
                ->setParameter('hack', '%hack%')
            ;
        }
        if ($hideEvents) {
            $qb
                ->andWhere('c.name NOT LIKE :event')
                ->setParameter('event', '%event%')
            ;
        }
        if ($consoleId) {
            $qb
                ->andWhere('c.id = :consoleId')
                ->setParameter('consoleId', $consoleId)
            ;
        }
        if ($searchValue) {
            $this->addSearch($searchValue, $qb);
        }

        return count($qb->getQuery()->getResult());
    }

    private function getFindOneByNameAndConsolePartialQuery(Console $console)
    {
        return $this->createQueryBuilder('g')
            ->join('g.achievements', 'a')
            ->andWhere('a.id is not null')
            ->andWhere('g.console = :console')
            ->setParameter('console', $console)
            ->setMaxResults(1)
        ;
    }

    public function findOneByNameAndConsole(string $name, string $epurName, Console $console): ?Game
    {
        $game = $this->getFindOneByNameAndConsolePartialQuery($console)
            ->andWhere('g.name LIKE :name')
            ->setParameter('name', $name)
            ->getQuery()
            ->getOneOrNullResult()
        ;
        if ($game) {
            return $game;
        }

        return $this->getFindOneByNameAndConsolePartialQuery($console)
            ->andWhere('g.epurName LIKE :epurName')
            ->setParameter('epurName', '%'.$epurName.'%')
            ->getQuery()
            ->getOneOrNullResult()
        ;
    }

    public function findTopPoints(int $max)
    {
        $qb = $this->createQueryBuilder('g')
            ->leftJoin('g.achievements', 'a')
            ->setMaxResults($max)
            ->addSelect('SUM(a.points) as totalPoints')
            ->orderBy('totalPoints', 'DESC')
            ->groupBy('g.id')
        ;
        $result = [];
        foreach ($qb->getQuery()->getResult() as $dat) {
            $result[] = $dat[0];
        }

        return $result;
    }

    public function findLastAdded(int $max)
    {
        $qb = $this->createQueryBuilder('g')
            ->leftJoin('g.achievements', 'a')
            ->groupBy('g.id')
            ->setMaxResults($max)
            ->addSelect('MIN(a.createdAt) as createdAt')
            ->orderBy('createdAt', 'DESC')
        ;
        $result = [];
        foreach ($qb->getQuery()->getResult() as $dat) {
            $result[] = $dat[0];
        }

        return $result;
    }

    public function findAllNoAchievement(string $consoleName = null)
    {
        $qb = $this->createQueryBuilder('g')
            ->leftJoin('g.achievements', 'a')
            ->groupBy('g.id')
            ->where('a.id is null')
        ;
        if ($consoleName) {
            $qb
                ->join('g.console', 'c')
                ->andWhere('c.name LIKE :consoleName')
                ->setParameter('consoleName', $consoleName)
            ;
        }

        return $qb->getQuery()->getResult();
    }
}
