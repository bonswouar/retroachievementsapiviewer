<?php

namespace App\Trait;

trait EpurStrTrait
{
    private function epurStr(string $str): string
    {
        $str = preg_replace(['@\(.*?\)@', '@\[.*?\]@', '@\~.*?\~@'], '', $str);
        $str = str_replace(':', ' ', $str);
        $str = str_replace('-', ' ', $str);
        $str = str_replace('|', ' ', $str);
        $str = str_replace('\'', ' ', $str);
        $str = str_replace('.', ' ', $str);
        $str = str_replace(',', ' ', $str);
        $str = str_replace('!', ' ', $str);
        $str = str_replace('&', ' ', $str);
        $str = preg_replace('/\s/', ' ', $str);
        $oldStr = null;
        while ($oldStr != $str) {
            $oldStr = $str;
            $str = str_replace('  ', ' ', $oldStr);
        }
        $str = trim($str);

        return $str;
    }
}
