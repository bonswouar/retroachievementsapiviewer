<?php

namespace App\Trait;

trait SafeFileGetContentsTrait
{
    private function safe_file_get_contents($url): string
    {
        $success = false;
        while (!$success) {
            try {
                $html = file_get_contents($url);
                $success = true;
            } catch (\Exception $e) {
                echo("\n".$e->getMessage()."\nWaiting for next retry...\n");
                if (str_contains($e->getMessage(), '429')) {
                    sleep(120);
                } else {
                    sleep(30);
                }
            }
        }
        return $html;
}
}
