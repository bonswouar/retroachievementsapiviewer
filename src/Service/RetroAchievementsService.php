<?php
namespace App\Service;

use App\Trait\SafeFileGetContentsTrait;

class RetroAchievementsService
{
    use SafeFileGetContentsTrait;

    private const API_URL = 'https://retroachievements.org/API/';
    private const RESULTS_CACHE_SIZE = 500;
    private $raUser;
    private $raApiKey;
    private $gameInfoExtendedCache = [];

    public function __construct($raUser, $raApiKey)
    {
        $this->raUser = $raUser;
        $this->raApiKey = $raApiKey;
    }

    private function cleanCache(&$cacheArrayVar)
    {
        if (count($cacheArrayVar) >= self::RESULTS_CACHE_SIZE) {
            $cacheArrayVar = [];
        }
    }

    private function authQS()
    {
        return '?z='.$this->raUser.'&y='.$this->raApiKey;
    }

    private function getRAURL($target, $params = '')
    {
        return $this->safe_file_get_contents(self::API_URL.$target.$this->authQS()."&$params");
    }

    public function getTopTenUsers()
    {
        return json_decode($this->getRAURL('API_GetTopTenUsers.php'), true, 512, JSON_THROW_ON_ERROR);
    }

    public function getGameInfo($gameID)
    {
        return json_decode($this->getRAURL('API_GetGame.php', "i=$gameID"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function getGameInfoExtended($gameID)
    {
        $this->cleanCache($this->gameInfoExtendedCache);
        if (!array_key_exists($gameID, $this->gameInfoExtendedCache)) {
            $this->gameInfoExtendedCache[$gameID] = json_decode($this->getRAURL('API_GetGameExtended.php', "i=$gameID"), true, 512, JSON_THROW_ON_ERROR);
        }

        return $this->gameInfoExtendedCache[$gameID];
    }

    public function getConsoleIDs()
    {
        return json_decode($this->getRAURL('API_GetConsoleIDs.php'), true, 512, JSON_THROW_ON_ERROR);
    }

    public function getGameList($consoleID)
    {
        return json_decode($this->getRAURL('API_GetGameList.php', "i=$consoleID"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function getFeedFor($user, $count, $offset = 0)
    {
        return json_decode($this->getRAURL('API_GetFeed.php', "u=$user&c=$count&o=$offset"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function getUserRankAndScore($user)
    {
        return json_decode($this->getRAURL('API_GetUserRankAndScore.php', "u=$user"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function getUserProgress($user, $gameIDCSV)
    {
        $gameIDCSV = preg_replace('/\s+/', '', $gameIDCSV);    // Remove all whitespace

        return json_decode($this->getRAURL('API_GetUserProgress.php', "u=$user&i=$gameIDCSV"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function GetUserRecentlyPlayedGames($user, $count, $offset = 0)
    {
        return json_decode($this->getRAURL('API_GetUserRecentlyPlayedGames.php', "u=$user&c=$count&o=$offset"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function GetUserSummary($user, $numRecentGames)
    {
        return json_decode($this->getRAURL('API_GetUserSummary.php', "u=$user&g=$numRecentGames&a=5"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function GetGameInfoAndUserProgress($user, $gameID)
    {
        return json_decode($this->getRAURL('API_GetGameInfoAndUserProgress.php', "u=$user&g=$gameID"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function GetAchievementsEarnedOnDay($user, $dateInput)
    {
        return json_decode($this->getRAURL('API_GetAchievementsEarnedOnDay.php', "u=$user&d=$dateInput"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function GetAchievementsEarnedBetween($user, $dateStart, $dateEnd)
    {
        $dateFrom = strtotime($dateStart);
        $dateTo = strtotime($dateEnd);

        return json_decode($this->getRAURL('API_GetAchievementsEarnedBetween.php', "u=$user&f=$dateFrom&t=$dateTo"), true, 512, JSON_THROW_ON_ERROR);
    }

    public function GetUserGamesCompleted($user)
    {
        return json_decode($this->getRAURL('API_GetUserCompletedGames.php', "u=$user"), true, 512, JSON_THROW_ON_ERROR);
    }
}
