<?php

declare(strict_types=1);

namespace DoctrineMigrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20220513113708 extends AbstractMigration
{
    public function getDescription(): string
    {
        return '';
    }

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('CREATE UNIQUE INDEX UNIQ_96737FF15FAAC8F6 ON achievement (ra_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_3603CFB65FAAC8F6 ON console (ra_id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_232B318C5FAAC8F6 ON game (ra_id)');
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->addSql('DROP INDEX UNIQ_96737FF15FAAC8F6 ON achievement');
        $this->addSql('DROP INDEX UNIQ_3603CFB65FAAC8F6 ON console');
        $this->addSql('DROP INDEX UNIQ_232B318C5FAAC8F6 ON game');
    }
}
